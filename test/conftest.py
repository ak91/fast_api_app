import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient
import alembic.config
import user_app.main


@pytest.fixture(autouse=True)
def apply_migrations():
    alembic.config.main(argv=["upgrade", "head"])
    yield
    alembic.config.main(argv=["downgrade", "base"])


@pytest.fixture(scope='function')
def app() -> FastAPI:
    return user_app.main.app


@pytest.fixture(scope='function')
def client(app):
    client = TestClient(app)
    yield client
