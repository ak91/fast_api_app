

def create_user_address_data(client):
    user_data = client.post(
        "/create_user/",
        json={"name": "john", "email": "test_email@gmail.com", "password": "secret_password"}).json()

    client.post(
        "/create_user/",
        json={"name": "alex", "email": "email@email", "password": "secret_password"})
    address_data = client.post("/create_address/", json={"name": "4 Privet Drive"}).json()
    client.post("/link_user_address/", json={"address_id": address_data["id"], "user_id": user_data["id"]})
    client.post("/create_address/", json={"name": "221B Baker St., London"}).json()


def test_list_users_when_no_users_present(client):
    response = client.get("/list_users/")
    assert response.status_code == 200
    assert response.json() == []


def test_list_user_with_no_address_present(client):
    client.post("/create_user/",
                json={"name": "alex", "email": "test_email@gmail.com", "password": "secret_password"})
    response = client.get("/list_users/")
    assert response.status_code == 200
    assert response.json() == [{'addresses': [], 'email': 'test_email@gmail.com', 'id': 1, 'name': 'alex'}]


def test_list_multiple_users_with_address(client):
    create_user_address_data(client)
    response = client.get("/list_users/")
    assert response.status_code == 200
    assert response.json() == [{'addresses': [{'id': 1, 'name': '4 Privet Drive'}],
                                'email': 'test_email@gmail.com', 'id': 1, 'name': 'john'},
                               {'addresses': [], 'email': 'email@email', 'id': 2, 'name': 'alex'}]


def test_list_addresses_when_no_address_present(client):
    response = client.get("/list_addresses/")
    assert response.status_code == 200
    assert response.json() == []


def test_list_address_with_no_user_present(client):
    client.post("/create_address/", json={"name": "10 Downing Street"})
    response = client.get("/list_addresses/")
    assert response.status_code == 200
    assert response.json() == [{'id': 1, 'name': '10 Downing Street', 'users': []}]


def test_list_mulitple_addresses_with_user(client):
    create_user_address_data(client)
    response = client.get("/list_addresses/")
    assert response.status_code == 200
    assert response.json() == [{'id': 1, 'name': '4 Privet Drive',
                                'users': [{'email': 'test_email@gmail.com', 'id': 1, 'name': 'john'}]},
                               {'id': 2, 'name': '221B Baker St., London', 'users': []}]
