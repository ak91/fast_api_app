from user_app import models, database


def test_post_to_create_address_does_create_address(client):
    assert len(next(database.get_db_session()).query(models.Address).all()) == 0
    response = client.post("/create_address/",
                           json={"name": "4 Privet Drive"})
    assert response.json() == {'name': '4 Privet Drive', 'users': [], 'id': 1}
    assert response.status_code == 200

    db_models = next(database.get_db_session()).query(models.Address).all()
    assert len(db_models) == 1
    assert db_models[0].name == '4 Privet Drive'
    assert db_models[0].users == []
