from user_app import models, database
from test import utils


def test_post_to_create_user_does_create_user(client):
    assert len(next(database.get_db_session()).query(models.User).all()) == 0
    response = client.post("/create_user/",
                           json={"name": "alex", "email": "test_email@gmail.com", "password": "secret_password"})
    assert response.status_code == 200
    assert response.json() == {'addresses': [], 'email': 'test_email@gmail.com', 'id': 1, 'name': 'alex'}
    db_models = next(database.get_db_session()).query(models.User).all()
    assert len(db_models) == 1
    assert db_models[0].email == 'test_email@gmail.com'
    assert db_models[0].name == 'alex'
    assert db_models[0].addresses == []
    assert utils.check_password("secret_password", db_models[0].password_hash)


def test_post_to_user_where_username_already_exists_does_give_sensible_error(client):
    client.post("/create_user/",
                json={"name": "alex", "email": "test_email@gmail.com", "password": "secret_password"})
    assert len(next(database.get_db_session()).query(models.User).all()) == 1

    response = client.post("/create_user/",
                           json={"name": "alex", "email": "bobby@gmail.com", "password": "secret_password"})
    assert response.status_code == 422
    assert response.json() == {"message": "user already exists"}
    assert len(next(database.get_db_session()).query(models.User).all()) == 1


def test_post_to_user_where_email_already_exists_does_give_sensible_error(client):
    client.post("/create_user/",
                json={"name": "alex", "email": "test_email@gmail.com", "password": "secret_password"})
    assert len(next(database.get_db_session()).query(models.User).all()) == 1

    response = client.post("/create_user/",
                           json={"name": "sam", "email": "test_email@gmail.com", "password": "secret_password"})
    assert response.status_code == 422
    assert response.json() == {"message": "email already exists"}
    assert len(next(database.get_db_session()).query(models.User).all()) == 1
