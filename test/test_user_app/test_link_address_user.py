from user_app import models, database


def test_does_link_user_to_address(client):
    user_data = client.post("/create_user/",
                            json={"name": "alex",
                                  "email": "test_email@gmail.com",
                                  "password": "secret_password"}).json()

    address_data = client.post("/create_address/",
                               json={"name": "4 Privet Drive"}).json()

    response = client.post("/link_user_address/",
                           json={"address_id": address_data["id"],
                                 "user_id": user_data["id"]})
    assert response.status_code == 200
    assert response.json() == {'result': 'ok'}
    session = next(database.get_db_session())
    assert len(session.query(models.User).all()) == 1
    assert len(session.query(models.Address).all()) == 1
    user_model = session.query(models.User).all()[0]
    address_model = session.query(models.Address).all()[0]

    assert len(user_model.addresses) == 1
    assert user_model.addresses[0].name == "4 Privet Drive"

    assert len(address_model.users) == 1
    assert address_model.users[0].name == "alex"
    assert address_model.users[0].email == "test_email@gmail.com"


def test_does_give_sensible_error_on_missing_user(client):
    address_data = client.post("/create_address/",
                               json={"name": "4 Privet Drive"}).json()

    response = client.post("/link_user_address/",
                           json={"address_id": address_data["id"],
                                 "user_id": 101})

    assert response.json() == {'message': 'user with that id does not exist'}
    assert response.status_code == 422


def test_does_give_sensible_error_on_missing_address(client):
    user_data = client.post("/create_user/",
                            json={"name": "alex",
                                  "email": "test_email@gmail.com",
                                  "password": "secret_password"}).json()

    response = client.post("/link_user_address/",
                           json={"address_id": 1234,
                                 "user_id": user_data['id']})

    assert response.json() == {'message': 'address with that id does not exist'}
    assert response.status_code == 422
