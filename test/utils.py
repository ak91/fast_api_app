import os
import sys
import bcrypt

FILE_PATH = os.path.abspath(__file__)
sys.path.append(os.path.dirname(os.path.dirname(FILE_PATH)))


def get_config_directory():
    return os.path.join(os.path.dirname(os.path.dirname(FILE_PATH)), "alembic.ini")


def check_password(plain_password: str, password_hash: str) -> bool:
    return bcrypt.checkpw(plain_password.encode(), password_hash.encode())
