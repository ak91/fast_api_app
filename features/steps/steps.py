import alembic.config
from behave import given, when, then


@given('the database is empty')
def step_impl(context):
    alembic.config.main(argv=["downgrade", "base"])
    alembic.config.main(argv=["upgrade", "head"])


@given('users and addresses are added to the database and linked')
def step_impl(context):
    user_data = context.test_client.post(
        "/create_user/",
        json={"name": "john", "email": "test_email@gmail.com", "password": "secret_password"}).json()

    context.test_client.post(
        "/create_user/",
        json={"name": "alex", "email": "email@email", "password": "secret_password"})
    address_data = context.test_client.post("/create_address/", json={"name": "4 Privet Drive"}).json()
    context.test_client.post("/link_user_address/",
                             json={"address_id": address_data["id"], "user_id": user_data["id"]})
    context.test_client.post("/create_address/", json={"name": "221B Baker St., London"}).json()


@when('a request is made to list all users')
def step_impl(context):
    context.test_request = context.test_client.get("/list_users/")


@then('a list of the users and their addresses should be returned')
def step_impl(context):
    assert context.test_request.json() == [{'addresses': [{'id': 1, 'name': '4 Privet Drive'}],
                                            'email': 'test_email@gmail.com', 'id': 1, 'name': 'john'},
                                           {'addresses': [], 'email': 'email@email', 'id': 2, 'name': 'alex'}]
