Feature: User-app request to list all users
Scenario:
  Given the database is empty
  Given users and addresses are added to the database and linked
  When a request is made to list all users
  Then a list of the users and their addresses should be returned