from fastapi.testclient import TestClient
import user_app.main

import alembic.config


def after_scenario(context, scenario):
    alembic.config.main(argv=["downgrade", "base"])


def before_scenario(context, scenario):
    context.test_client = TestClient(user_app.main.app)
