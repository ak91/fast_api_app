# README #


### What is it for ###

FastAPI demonstration app implmenting the following functionality:

* Create user
* Create address
* Link address to user (or user to address)
* List all users
* List all addresses

### How to set up ###

* Python version tested on: 3.8.9
* Install dependencies with following command: pip install -r requirements.txt
* Create a database migration with: alembic revision --autogenerate -m "first commit"
* Run the following command: alembic upgrade head
* Running pytest tests with: pytest test
* Running behave tests with: behave


### Who to talk to ###

* Repo owner: Alexander Kitt
