from __future__ import annotations
import bcrypt
from typing import List
from sqlalchemy.orm import Session

from user_app import models, schemas, exceptions


def hash_password(plain_password: str) -> str:
    return bcrypt.hashpw(plain_password.encode(), bcrypt.gensalt()).decode()


def check_password(plain_password: str, password_hash: str) -> bool:
    return bcrypt.checkpw(plain_password.encode(), password_hash.encode())


def create_user(db: Session, user: schemas.UserCreate) -> models.User:
    db_user = models.User(email=user.email, name=user.name, password_hash=hash_password(user.password))
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def create_address(db: Session, address: schemas.AddressCreate) -> models.Address:
    address = models.Address(name=address.name)
    db.add(address)
    db.commit()
    db.refresh(address)
    return address


def create_address_user_link(db: Session, address_user_link: schemas.AddressUserLinkCreate) -> None:
    address = db.query(models.Address).get(address_user_link.address_id)

    if address is None:
        raise exceptions.AddressDoesNotExistError

    user = db.query(models.User).get(address_user_link.user_id)

    if user is None:
        raise exceptions.UserDoesNotExistError

    address.users.append(user)
    user.addresses.append(address)
    db.commit()
    db.refresh(address)
    db.refresh(user)


def list_users(db: Session) -> List[models.User]:
    return db.query(models.User).all()


def list_addresses(db: Session) -> List[models.Address]:
    return db.query(models.Address).all()
