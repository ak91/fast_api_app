from __future__ import annotations
from typing import List
from fastapi import Request, Depends, FastAPI
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session

from user_app import schemas, database, exceptions, service


def get_application():
    return FastAPI()


app = get_application()


@app.exception_handler(exceptions.UserAlreadyExistsError)
def user_already_exists_handler(request: Request, exception: exceptions.UserAlreadyExistsError):
    return JSONResponse(status_code=422, content={"message": "user already exists"})


@app.exception_handler(exceptions.EmailAlreadyExistsError)
def email_already_exists_handler(request: Request, exception: exceptions.EmailAlreadyExistsError):
    return JSONResponse(status_code=422, content={"message": "email already exists"})


@app.exception_handler(exceptions.UserDoesNotExistError)
def user_not_found_error(request: Request, exception: exceptions.UserDoesNotExistError):
    return JSONResponse(status_code=422, content={"message": "user with that id does not exist"})


@app.exception_handler(exceptions.AddressDoesNotExistError)
def address_not_found_error(request: Request, exception: exceptions.AddressDoesNotExistError):
    return JSONResponse(status_code=422, content={"message": "address with that id does not exist"})


@app.post("/create_user/", response_model=schemas.UserListAddress)
def create_user(user: schemas.UserCreate, db: Session = Depends(database.get_db_session)):
    db_user = service.UserService(db).create_user(user)
    return db_user


@app.post("/create_address/", response_model=schemas.AddressListUser)
def create_address(address: schemas.AddressCreate, db: Session = Depends(database.get_db_session)):
    address = service.UserService(db).create_address(address)
    return address


@app.post("/link_user_address/", response_model=schemas.AddressListUser)
def link_user_address(address_user_link: schemas.AddressUserLinkCreate,
                      db: Session = Depends(database.get_db_session)):
    service.UserService(db).link_address_to_user(address_user_link)
    return JSONResponse({'result': 'ok'})


@app.get("/list_users/", response_model=List[schemas.UserListAddress])
def list_users(db: Session = Depends(database.get_db_session)):
    return service.UserService(db).list_users()


@app.get("/list_addresses/", response_model=List[schemas.AddressListUser])
def list_addresses(db: Session = Depends(database.get_db_session)):
    return service.UserService(db).list_addresses()
