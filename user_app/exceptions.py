

class UserAlreadyExistsError(Exception):
    pass


class EmailAlreadyExistsError(Exception):
    pass


class UserDoesNotExistError(Exception):
    pass


class AddressDoesNotExistError(Exception):
    pass
