from sqlalchemy import Column, ForeignKey, Integer, String, Table
from sqlalchemy.orm import relationship

from user_app import database


user_address_association_table = Table('association',
                                       database.Base.metadata,
                                       Column('user_id', Integer, ForeignKey('user.id')),
                                       Column('address_id', Integer, ForeignKey('address.id')))


class User(database.Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, index=True)
    email = Column(String, unique=True, index=True)
    password_hash = Column(String)
    addresses = relationship("Address",
                             secondary=user_address_association_table,
                             back_populates="users")


class Address(database.Base):
    __tablename__ = 'address'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    users = relationship("User",
                         secondary=user_address_association_table,
                         back_populates="addresses")
