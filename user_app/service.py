from __future__ import annotations
from typing import TYPE_CHECKING, List
from sqlalchemy import exc
from user_app import crud, exceptions
from sqlalchemy.orm import Session

if TYPE_CHECKING:
    from schemas import AddressCreate, UserCreate, AddressUserLinkCreate
    from models import Address, User


class UserService:

    def __init__(self, db: Session):
        self.db = db

    def create_user(self, user: UserCreate) -> User:
        try:
            return crud.create_user(self.db, user)
        except exc.IntegrityError as error:
            if 'UNIQUE constraint failed: user.name' in str(error):
                raise exceptions.UserAlreadyExistsError
            elif 'UNIQUE constraint failed: user.email' in str(error):
                raise exceptions.EmailAlreadyExistsError
            else:
                raise

    def create_address(self, address: AddressCreate) -> Address:
        return crud.create_address(self.db, address)

    def link_address_to_user(self, address_user_link: AddressUserLinkCreate) -> None:
        return crud.create_address_user_link(self.db, address_user_link)

    def list_users(self) -> List[User]:
        return crud.list_users(self.db)

    def list_addresses(self) -> List[Address]:
        return crud.list_addresses(self.db)
