from __future__ import annotations
from typing import List, Optional, ForwardRef
from pydantic import BaseModel


class UserBase(BaseModel):
    name: str
    email: str


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int

    class Config:
        orm_mode = True


class UserListAddress(UserBase):
    id: int
    addresses: Optional[ForwardRef('List[Address]')]

    class Config:
        orm_mode = True


class AddressBase(BaseModel):
    name: str


class AddressCreate(AddressBase):
    pass


class Address(AddressBase):
    id: int

    class Config:
        orm_mode = True


class AddressListUser(AddressBase):
    id: int
    users: Optional[List[User]]

    class Config:
        orm_mode = True


class AddressUserLinkCreate(BaseModel):
    address_id: int
    user_id: int


UserListAddress.update_forward_refs()
